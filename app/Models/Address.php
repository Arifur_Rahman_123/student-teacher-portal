<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Address extends Model{

    use HasFactory, Notifiable;

    protected $table = 'address';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
    'house_no',
    'road_no',  
    'city',
    'country',
    'updated_at',
    'created_at'
   ];

}


