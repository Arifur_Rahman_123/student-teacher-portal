<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Mail\NotifyMail;
use Exception;
use DB;
use Validator;
use Mail;
use Storage;

class UserController extends Controller
{
    /**
     * Update the specific resource's password
     *
     * @param \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

     public function updatePassword( Request $request ){

        try{
            $rules = array(
                'user_id'          => 'required',
                'old_password'     => 'required',
                'new_password'     => 'required',
                'confirm_password' => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }
            $id = $request->user_id;
            $old_password = $request->old_password;

            $user = User::select('password')
            ->where('user_id',$id)->first();
            // return $user;
            if (!Hash::check($old_password,$user->password)) {
            throw new Exception('Old Password not correct.');
        }

            $password = Hash::make($request->new_password);
            User::where('user_id',$id)->update(['password' => $password]);
            return back()->with('success','Password changed successfully.');
        }
        catch (Exception $e) {
            return back()->with('error',$e->getMessage());
        }
     }

     public function getUser($id){
        try{
            $show_user = DB::table('users')
            ->join('address','address.user_id','=','users.user_id')
            ->Select('users.user_id','users.username','users.first_name','users.user_image_path','users.last_name','users.email','users.phone','address.house_no','address.road_no','address.city','address.country')
            ->where('users.user_id', $id)
            ->first();
            if (!$show_user) {
                throw new Exception('User doesnot exist!');
            }

            return response()->json(array(
                'status' => true,
                'user' => $show_user,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
     }



     public function getTeacher($id){
        try{
            $show_user = DB::table('users')
            ->join('address','address.user_id','=','users.user_id')
            ->Select('users.user_id','users.user_image_path','users.total_students','users.total_courses','users.username','users.first_name','users.last_name','users.rating','users.email','users.profile_bio','users.phone','address.house_no','address.road_no','address.city','address.country')
            ->where('users.user_id', $id)
            ->first();
            if (!$show_user) {
                throw new Exception('User doesnot exist!');
            }

            return response()->json(array(
                'status' => true,
                'user' => $show_user,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
     }



     public function getStudent($id){
        try{
            $show_user = DB::table('users')
            ->join('address','address.user_id','=','users.user_id')
            ->Select('users.user_id','users.user_image_path','users.username','users.first_name','users.last_name','users.email','users.phone','address.house_no','address.road_no','users.profile_bio','address.city','address.country')
            ->where('users.user_id', $id)
            ->first();
            if (!$show_user) {
                throw new Exception('User doesnot exist!');
            }

            $completed_courses = DB::table('courses')
            ->join('course_students','course_students.course_id','=','courses.course_id')
            ->Select('courses.course_name')
            ->where('course_status','=','completed')
            ->where('course_students.student_id', $id)
            ->first();

            $running_courses = DB::table('courses')
            ->join('course_students','course_students.course_id','=','courses.course_id')
            ->Select('courses.course_name')
            ->where('course_status','=','available')
            ->where('course_students.student_id', $id)
            ->first();



            return response()->json(array(
                'status' => true,
                'user' => $show_user,
                'completed' => $completed_courses,
                'running' => $running_courses
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
     }

     /**
      * Updating the user profile.
      * there will be two parameter in the payload
      */

     public function update(Request $request, $id)
     {
         try{
             $rules = array(
                 'username'  => 'required',
                 'password'     => 'required',
                 'role'     => 'required',
                 'first_name'  => 'required',
                 'last_name'   => 'required',
                 'email'         => 'required',
                 'phone'         => 'required',
                 'address'       => 'required',
             );
             $validator = Validator::make($request->all(), $rules);
             if (!$validator->passes()) {
                 throw new Exception('All fields are required');
             }

             $update_user['username'] = $request->username;
             $update_user['first_name'] = $request->first_name;
             $update_user['last_name'] = $request->last_name;
             $update_user['email'] = $request->email;
             $update_user['phone'] = $request->phone;
             $update_user['address'] = $request->address;
             $update_user['password'] = Hash::make($request->password);

             $update = User::where('user_id', $id)
                 ->update($update_user);
             if (!$update) {
                 throw new Exception('Update user failed!');
             }

             return response()->json(array(
                 'status' => true,
                 'status_message' => "User Update Successful!",
                 // 'teacher' => $update,
             ));
         }
         catch (Exception $e) {
             return response()->json(array(
                 'status' => false,
                 'status_message' => $e->getMessage(),
             ));
         }
     }


     /**
      * Teacher Pending List Fetching
      */

      public function getPendingList(){
       try {
        $get_pendings=DB::table('users')
        ->join('address','address.user_id','=','users.user_id')
        ->Select('users.user_id','users.username','users.first_name','users.last_name','users.email','users.phone','users.wallet_amount','address.house_no','address.road_no','address.city','address.country')
        // ->select('user_id','username','first_name','last_name','email','phone','address','wallet_amount','teacher_designation')
        ->Where('isPending','1')
        ->get();

        if(!$get_pendings){
          throw new Exception('getting pending list failed');
        }

        return response()->json(array(
          'status' => true,
          'teachers' => $get_pendings,
          // 'teacher' => $update,
      ));
       } catch (Exception $e) {
        return response()->json(array(
            'status' => false,
            'status_message' => $e->getMessage(),
        ));
       }
      }

      /**
       * get Active teacher list
       */
      public function getActiveList(){
        try {
         $get_actives=DB::table('users')
         ->join('address','address.user_id','=','users.user_id')
         ->Select('users.user_id','users.username','users.first_name','users.last_name','users.wallet_amount','users.email','users.phone','address.house_no','address.road_no','address.city','address.country')
        //  ->select('user_id','username','first_name','last_name','email','phone','wallet_amount','teacher_designation')
         ->Where('isPending','0')
         ->get();

         if(!$get_actives){
           throw new Exception('getting active list failed');
         }

         return response()->json(array(
           'status' => true,
           'teachers' => $get_actives,
           // 'teacher' => $update,
       ));
        } catch (Exception $e) {
         return response()->json(array(
             'status' => false,
             'status_message' => $e->getMessage(),
         ));
        }
       }


       /*
       *Store new Teacher
       */

      public function createNewUser(Request $request)
      {
          try{
              $rules = array(
                  'username'  => 'required',
                  'password'     => 'required',
                  'role'     => 'required',
                  'first_name'  => 'required',
                  'last_name'   => 'required',
                  'user_image_path'=>'required',
                  'email'         => 'required',
                  'phone'         => 'required',
                  'house_no'       => 'required',
                  'road_no'       => 'required',
                  'city'       => 'required',
                  'country'       => 'required',
                  'teacher_designation' => 'required'
              );
              $validator = Validator::make($request->all(), $rules);
              if (!$validator->passes()) {
                  throw new Exception('All fields are required');
              }

              $full_name = $request->first_name." ".$request->last_name;
              $password = $request->password;
              $role = strtolower($request->role);
              $email = $request -> email;
              $insert_user['username'] = $request->username;
              $insert_user['first_name'] = $request->first_name;
              $insert_user['last_name'] = $request->last_name;
              $insert_user['email'] = $request->email;
              $insert_user['phone'] = $request->phone;
              $insert_user['user_image_path'] = $request->user_image_path;
              $insert_user['role'] = strtolower($request->role);
              $insert_user['wallet_amount'] = 0;
              $insert_user['isPending'] = 0;
              $insert_user['password'] = Hash::make($request->password);
              if($request->role == 'teacher'){
              $insert_user['teacher_designation'] = $request->teacher_designation;
              }else{
              $insert_user['teacher_designation'] = '';
              }

              $insert_user_DB = DB::table('users')->insertGetId($insert_user);

              $insert_user_address['user_id'] = $insert_user_DB;
              $insert_user_address['house_no'] = $request->house_no;
              $insert_user_address['road_no'] = $request->road_no;
              $insert_user_address['city'] = $request->city;
              $insert_user_address['country'] = $request->country;
              $insert_user_address_DB = DB::table('address')->insert($insert_user_address);

              if(!$insert_user_DB){
                throw new Exception('Inserting failed');
              }

              if(!$insert_user_address_DB){
                throw new Exception('Inserting user address failed');
              }

              if($role == 'teacher'){
                $email_body="You have been assigned as a teacher in hive lms.Please complete your profile after first login";
                Mail::to($email)->send(new NotifyMail($request->username,$full_name,$email_body,$password));
                    if (Mail::failures()) {
                        return response()->Fail('Sorry! Please try again latter');
                    }else{
                        return response()->json(array(
                            'status' => true,
                            'status_message' => "Teacher Created Successful!",
                            'mail_sent' => true,
                        ));
                    }
            }
            else if($request->role == 'student'){
                $email_body="You have been enrolled as a student in hive lms.Please complete your profile after first login";
                Mail::to($email)->send(new NotifyMail($request->username,$full_name,$email_body,$password));
                    if (Mail::failures()) {
                        return response()->Fail('Sorry! Please try again latter');
                    }else{
                        return response()->json(array(
                            'status' => true,
                            'status_message' => "Student Created Successful!",
                            'mail_sent' => true,
                        ));
                    }
              }
              else if($request->role == 'student'){
                return response()->json(array(
                    'status' => true,
                    'status_message' => "Admin Created Successful!",
                    'admin_info' => $insert_user_DB,
                ));
              }
              else{
                return response()->json(array(
                    'status' => true,
                    'status_message' => "Super Admin Created Successful!",
                    'super_admin_info' => $insert_user_DB,
                ));
              }
          }
          catch (Exception $e) {
              return response()->json(array(
                  'status' => false,
                  'status_message' => $e->getMessage(),
              ));
          }
      }








      public function deleteUser($id){

           try{

            $deleteFromDB=DB::table('users')
            ->where('user_id',$id)
            ->delete();

            if(!$deleteFromDB){
                throw new Exception('Deleting failed');
            }
            return response()->json(array(
                'status' => true,
                'status_message' => "Delete Successful!",
                'super_admin_info' => $deleteFromDB,
            ));
           }catch(Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
      }
    }

    public function dispatchTeacher(Request $request){

        try{

            $id=$request->user_id;

            $selected_teacher=DB::table('users')
            ->where('user_id',$id)
            ->get();

        $selected_teacher->toArray();

        $isPending='0';
        $is_pending['isPending']=$isPending;
        $email=$selected_teacher[0]->email;
        $firstName=$selected_teacher[0]->first_name;
        $lastName=$selected_teacher[0]->last_name;
        $password=$selected_teacher[0]->password;
        $fullName=$firstName." ".$lastName;
         $update_pendings=DB::table('users')
         ->where('user_id',$id)
         ->update($is_pending);

         if(!$update_pendings){
             throw new Exception('Deleting failed');
         }
         return response()->json(array(
             'status' => true,
             'status_message' => "Dispatching Successful!",
             'super_admin_info' => $update_pendings,
         ));



         $email_body="You have been assigned as a teacher in hive lms.Please complete your profile after first login";
         Mail::to($email)->send(new NotifyMail($request->username,$full_name,$email_body,$password));
            if (Mail::failures()) {
                return response()->Fail('Sorry! Please try again latter');
            }else{
                return response()->json(array(
                    'status' => true,
                    'status_message' => "Teacher Created Successful!",
                    'mail_sent' => true,
                ));
            }
            }catch(Exception $e) {
                return response()->json(array(
                    'status' => false,
                    'status_message' => $e->getMessage(),
                ));
        }
    }
 }


//////New change/////////
