<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Course;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DatePeriod;
use DateTime;
use DateInterval;
use Exception;
use Validator;
use DB;

// use Carbon\Carbon;

class CourseController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAllCourses()
    {
        try{
            $show_all_courses = DB::table('courses')
            ->join('users', 'courses.teacher_id', '=', 'users.user_id')
            ->select('courses.course_id','courses.course_name','courses.course_image_path','courses.discount_percentage','courses.max_students','courses.course_image_path','courses.rating','courses.student_count','users.email','users.first_name','users.last_name','courses.course_description','courses.course_price','courses.type')->paginate(10);

            $result = $show_all_courses->toArray();

            return response()->json(array(
                'status' => true,
                'current_page'=>$result["current_page"],
                'total_page' => $result["last_page"],
                'courses' => $result["data"],
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }





     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function displayCourseDetails($id)
    {
        try{
            $specific_course = Course::where('course_id', $id)
                ->first();
            if (!$specific_course) {
                throw new Exception('Course doesnot exist!');
            }

            $display_course_info = DB::table('courses')
            ->join('categories', 'courses.category_id', '=', 'categories.category_id')
            ->select('courses.course_id','courses.course_name','courses.course_image_path','courses.max_students','courses.student_count','courses.start_date','courses.end_date','courses.course_description','courses.requirements','courses.discount_percentage','courses.course_price','courses.rating','categories.category_name','courses.type')
            ->where('courses.course_id',$id)
            ->get();

            $display_teacher_info = DB::table('courses')
            ->join('users', 'courses.teacher_id', '=', 'users.user_id')
            ->select('users.user_id','users.username','users.first_name','users.teacher_designation','users.user_image_path','users.last_name',)
            ->where('courses.course_id',$id)
            ->get();

            $display_lecture_info = DB::table('lectures')
            ->Where('course_id',$id)
            ->get();

            $total_hours = DB::table('lectures')
            ->select(DB::raw('SUM(hours) as total_hours'))
            ->Where('course_id',$id)
            ->get();

            //DB::raw('SUM(quiz_results.availed_marks) as total_marks')

            $display_comments=DB::table('comments')
            ->join('courses', 'comments.course_id', '=', 'courses.course_id')
            ->join('users', 'users.user_id', '=', 'comments.user_id')
            ->select('comments.comment_text','comments.rating','users.username','users.first_name','users.user_image_path','users.last_name')
            ->where('courses.course_id',$id)
            ->get();


            if (!$display_course_info) {
                throw new Exception('Course details fetching failed!');
            }

            return response()->json(array(
                'status' => true,
                'message'=> 'course details fetching successful',
                'course' => $display_course_info,
                'teacher'=> $display_teacher_info,
                'lectures'=> $display_lecture_info,
                'total_hourse'=>$total_hours,
                'comments'=> $display_comments
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    public function displayCoursesByCategory($id)
    {
        try{
            $specific_course = Course::where('category_id', $id)
                ->first();
            if (!$specific_course) {
                throw new Exception('Category_id doesnot exist!');
            }

            $display_specific_course_list_category = DB::table('courses')
            ->join('categories','courses.category_id', '=', 'categories.category_id')
            ->join('users', 'courses.teacher_id', '=', 'users.user_id')
            ->select('courses.course_id','courses.course_name','courses.course_image_path','courses.discount_percentage','users.first_name','users.last_name','courses.rating','courses.course_price','courses.course_status','courses.type')
            ->where('courses.category_id',$id)->paginate(10);

            $collection = $display_specific_course_list_category->getCollection();
            if (!$display_specific_course_list_category) {
                throw new Exception('Courses By Category fetching failed!');
            }

            return response()->json(array(
                'messages'=>'Courses By Category fetching is successful!',
                'status' => true,
                'course' => $collection,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Display course count By catgeory
     */
    public function displayCoursesCountByCategory()
    {
        try{
            // $specific_course = Course::where('category_id', $id)
            //     ->first();
            // if (!$specific_course) {
            //     throw new Exception('Course doesnot exist!');
            // }

            $display_course_count_by_category =  DB::table('courses')
                                                 ->join('categories','courses.category_id', '=', 'categories.category_id')
                                                 ->select('categories.category_id','categories.category_name','categories.category_image_path', DB::raw('count(*) as total'))
                                                 ->groupBy('courses.category_id')->paginate(10);

            $collection = $display_course_count_by_category->getCollection();
            if (!$display_course_count_by_category) {
                throw new Exception('Course counts by Category successful!');
            }

            return response()->json(array(
                'status' => true,
                'message'=> 'Course counts by Category successful!',
                'categories' => $collection,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }




    /**
    * Creating a course
    *The way was bit complexed as we went through some well amount of joins and loops
    */


    public function createCourse(Request $request)
    {
        try{
            // $rules = array(
            //     'category_id'            => 'required',
            //     'teacher_id'             => 'required',
            //     'course_name'            => 'required',
            //     'course_description'     => 'required',
            //     'course_price'           => 'required',
            //     'type'                   => 'required',
            //     'min_students'           => 'required',
            //     'max_students'           => 'required',
            //     'start_date'             => 'required',
            //     'end_date'               => 'required',
            //     'schedule'               => 'required',
            //  );
            // $validator = Validator::make($request->all(), $rules);
            // if (!$validator->passes()) {
            //     throw new Exception('All fields are required');
            // }


            $start=$request->start_date;
            $end=$request->end_date;
            $teacher_id_taken=$request->teacher_id;
            $insert_course['category_id'] = $request->category_id;
            $insert_course['teacher_id'] = $request->teacher_id;
            $insert_course['course_name'] = $request->course_name;
            $insert_course['course_description'] = $request->course_description;
            $insert_course['course_price'] = $request->course_price;
            $insert_course['student_count'] = 0;
            $insert_course['type'] = $request->type;
            $insert_course['min_students'] = $request->min_students;
            $insert_course['max_students'] = $request->max_students;
            $insert_course['start_date'] = $request->start_date;
            $insert_course['end_date'] = $request->end_date;
            $insert_course['requirements'] = $request->course_requirement;
            $insert_course['discount_percentage'] = $request->discount_percentage;
            $insert_course['course_status'] = 'available';
            $insert_course['rating'] = 0;
            $insert_course['student_count'] = 0;

            $course_image_destination = 'images';

            if ($request->has('course_image_path')) {
                if ($request->hasFile('course_image_path')) {
                    $document = $request->file('course_image_path');
                    $documentName = Storage::disk('local')->put($course_image_destination, $document);


                    $insert_course['course_image_path'] = $documentName;
                }

            };


            $destination = 'files';
            if ($request->has('certificate')) {
                if ($request->hasFile('certificate')) {
                    $documentCertificate = $request->file('certificate');
                    $documentNameCertificate = Storage::disk('local')->put($destination, $documentCertificate);
                    $insert_course['certificate'] = $documentNameCertificate;
                }

            };


            //In the next line we will insert our course.But we need the id instantly thats why we have used insertGetId instead of insert;
            $create_course_id = Course::insertGetId($insert_course);

            //Taking out the schedule array that we have got from the frontend.
            $json_schedule =json_decode($request->schedule, true);

            //The next phase we will calculate the days between the start and end date to find out the actual numbers of days.
            $format = 'Y-m-d';
            $array = array();
            $class_days = array();
            $interval = new DateInterval('P1D');

            $realEnd = new DateTime($end);
            $realEnd->add($interval);

            $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

            //Taking out the schedule array and inserting it in course_timing table
            for($i=0;$i<sizeof($json_schedule);$i++){
                $insert_schedule['course_id'] = $create_course_id;
                $insert_schedule['day'] = $json_schedule[$i]['day'];
                $insert_schedule['start_time'] = $json_schedule[$i]['start_time'];
                $insert_schedule['end_time'] = $json_schedule[$i]['end_time'];
                //We need its immediate id too.
                $insert_schedule_DB = DB::table('course_timing')->insertGetId($insert_schedule);
                if(!$insert_schedule_DB){
                   throw new Exception('schedule inserting failed');
                }
                //now loop through the previously created period array of dates.And MAke proper format of those dates as per your requirement.And insert them in the DB.
                foreach($period as $date) {
                   $coverted_date = $date->format($format);
                   $day = Carbon::createFromFormat('Y-m-d', $coverted_date)->format('l');
                   if(strtolower($day) ==strtolower($json_schedule[$i]['day'])){
                    $insert_class['course_id']=$create_course_id;
                    $insert_class['timing_id']=$insert_schedule_DB;
                    $insert_class['teacher_id']=$teacher_id_taken;
                    $insert_class['start_time']=$json_schedule[$i]['start_time'];
                    $insert_class['end_time']=$json_schedule[$i]['end_time'];
                    $insert_class['day']=$day;
                    $insert_class['date']=$coverted_date;
                    $insert_classes_DB = DB::table('classes')->insert($insert_class);
                   }
                }

            }

            return response()->json(array(
                'status' => true,
                'status_message' => "Course Create Successful!",
                'course' => $create_course_id,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

     /**
     * Searching course by searchKey
     *
     * @param  string  $searchKey
     * @return \Illuminate\Http\Response
     */
    public function displayCourseFromSearchKey($searchKey)
    {
        try{
            $display_courses_by_searchKey = DB::table('courses')
            ->join('users', 'courses.teacher_id', '=', 'users.user_id')
            ->where('courses.course_name',$searchKey)
            ->get();


            if (!$display_courses_by_searchKey) {
                throw new Exception('Course details fetching failed!');
            }

            return response()->json(array(
                'status' => true,
                'courses' => $display_courses_by_searchKey,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * retrieve all the subject available in the database
     */

    public function getAllCategories(){

    try{
        $display_subjects = DB::table('categories')->paginate(10);


        if (!$display_subjects) {
            throw new Exception('Categories fetching failed!');
        }


        return response()->json(array(
            'status' => true,
            'message'=>'Category fetching successful',
            'subjects' => $display_subjects,
        ));

    }
    catch (Exception $e) {
        return response()->json(array(
            'status' => false,
            'status_message' => $e->getMessage(),
        ));
    }

}

public function calculateClasses(Request $request){

     try {

        $rules = array(
            'start_date'            =>  'required',
            'end_date'              => 'required',
        );

        $format = 'Y-m-d';

        $start=$request->start_date;
        $end=$request->end_date;
       $array = array();
       $class_days = array();

       // Variable that store the date interval
       // of period 1 day
       $interval = new DateInterval('P1D');

       $realEnd = new DateTime($end);
       $realEnd->add($interval);

       $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

       // Use loop to store date into array
       foreach($period as $date) {
           $coverted_date = $date->format($format);
           $day = Carbon::createFromFormat('Y-m-d', $coverted_date)->format('l');
           if(strtolower($day) =='monday'| strtolower($day) =='tuesday' ){
            $list[] = array(
                'date' => $coverted_date,
                'day' => $day,
            );
           }
       }
        // $day = Carbon::createFromFormat('m-d-Y', $start_date)->format('l');

        return response()->json(array(
            'status' => true,
            'message'=>'Category fetching successful',
            'subjects' => $list,
        ));
     } catch (Exception $e) {
        return response()->json(array(
            'status' => false,
            'status_message' => $e->getMessage(),
        ));
     }
    }

    public function showAllCoursesForWeb(Request $request){
        try{

            $show_all_courses = DB::table('courses')
            ->join('users', 'courses.teacher_id', '=', 'users.user_id')
            ->select('courses.course_id','courses.course_name','courses.max_students','courses.course_image_path','courses.rating','courses.student_count','users.email','users.first_name','users.last_name','courses.course_description','courses.course_price','courses.type')
            ->get();

            // $result = $show_all_courses->toArray();

            return response()->json(array(
                'status' => true,

                'courses' => $show_all_courses,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }

    }

    public function showTopCourses()
    {
        try{
            $show_all_courses = DB::table('courses')
            ->join('users', 'courses.teacher_id', '=', 'users.user_id')
            ->select('courses.course_id','courses.course_name','courses.course_image_path','courses.discount_percentage','courses.max_students','courses.course_image_path','courses.rating','courses.student_count','users.email','users.first_name','users.last_name','courses.course_description','courses.total_selling','courses.course_price','courses.type')->paginate(10);

            $result = $show_all_courses->toArray();

            return response()->json(array(
                'status' => true,
                'current_page'=>$result["current_page"],
                'total_page' => $result["last_page"],
                'courses' => $result["data"],
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }



}
