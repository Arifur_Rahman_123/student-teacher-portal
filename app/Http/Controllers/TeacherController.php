<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\File;
use App\Models\User;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Datetime;
use Validator;

class TeacherController extends Controller
{
    /**
     * The following method is for fetching courses taught by a specific teacher
     */
    public function showTeachingCourses($id)
    {

        try {
            $courses_taught_by_a_teacher = Course::where('teacher_id', $id)
                ->get();
            if (!$courses_taught_by_a_teacher) {
                throw new Exception('Teacher currently doesnot have any course to teach');
            }

            return response()->json(array(
                'status' => true,
                'courses' => $courses_taught_by_a_teacher,
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }

    }

    public function showRunningCourses($id)
    {

        try {
            $courses_taught_by_a_teacher = Course::where('teacher_id', $id)
                ->where('course_status','=','available')
                ->get();
            if (!$courses_taught_by_a_teacher) {
                throw new Exception('Teacher currently doesnot have any course to teach');
            }

            return response()->json(array(
                'status' => true,
                'courses' => $courses_taught_by_a_teacher,
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }

    }



    /**
     * Update the specific resource's password
     *
     * @param \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function updatePassword(Request $request)
    {

        try {
            $rules = array(
                'user_id' => 'required',
                'old_password' => 'required',
                'new_password' => 'required',
                'confirm_password' => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }
            $id = $request->user_id;
            $old_password = $request->old_password;

            $user = User::select('password')
                ->where('user_id', $id)->first();
            // return $user;
            if (!Hash::check($old_password, $user->password)) {
                throw new Exception('Old Password not correct.');
            }

            $password = Hash::make($request->new_password);
            User::where('user_id', $id)->update(['password' => $password]);
            return back()->with('success', 'Password changed successfully.');
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function getUser($id)
    {
        try {
            $show_user = User::where('user_id', $id)
                ->first();
            if (!$show_user) {
                throw new Exception('User doesnot exist!');
            }

            return response()->json(array(
                'status' => true,
                'user' => $show_user,
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    public function getAllTeachers()
    {
        try {
            $show_all_teachers = DB::table('users')
                ->join('address','address.user_id','=','users.user_id')
                ->select('users.user_id','users.username', 'users.first_name','users.user_image_path','users.teacher_designation', 'users.last_name', 'users.email', 'users.phone','users.profile_bio','users.total_students','users.total_courses','users.rating', 'address.house_no','address.road_no','address.city','address.country')
                ->where('role', 'teacher')->paginate(10);
            $result = $show_all_teachers->toArray();
            if (!$show_all_teachers) {
                throw new Exception('User doesnot exist!');
            }
            return response()->json(array(
                'status' => true,
                'current_page' => $result["current_page"],
                'total_page' => $result["last_page"],
                'teachers' => $result["data"],
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Updating the user profile.
     * there will be two parameter in the payload
     */

    public function updateProfile(Request $request,$id)
    {


        try {



            $update_user = array();
            $update_address = array();
            $destination = 'images';

            if ($request->has('user_image_path')) {
                if ($request->hasFile('user_image_path')) {
                    $image = $request->file('user_image_path');
                    $imageName = Storage::disk('local')->put($destination, $image);
                    $update_user['user_image_path'] = "/".$imageName;

                }

            };

            if ($request->has('username')) {
                $update_user['username'] = $request->username;
            }
            if ($request->has('first_name')) {
                $update_user['first_name'] = $request->first_name;
            }

            if ($request->has('last_name')) {
                $update_user['last_name'] = $request->last_name;
            }

            if ($request->has('email')) {
                $update_user['email'] = $request->email;
            }

            if ($request->has('phone')) {
                $update_user['phone'] = $request->phone;
            }
            if ($request->has('gender')) {
                $update_user['gender'] = $request->gender;
            }
            if ($request->has('role')) {
                $update_user['role'] = $request->role;
            }

            if ($request->has('profile_bio')) {
                $update_user['profile_bio'] = $request->profile_bio;
            }

            if ($request->has('house_no')) {
                $update_address['house_no'] = $request->house_no;
            }

            if ($request->has('road_no')) {
                $update_address['road_no'] = $request->road_no;
            }

            if ($request->has('city')) {
                $update_address['city'] = $request->city;
            }

            if ($request->has('country')) {
                $update_address['country'] = $request->country;
            }

            if($update_user != []){
                $update_user_DB = DB::table('users')
                ->where('user_id', $id)
                ->update($update_user);
            }

            if($update_address != []){
                $update_address_DB = DB::table('address')
                ->where('user_id', $id)
                ->update($update_address);
            }

            // if (!$update_user_DB) {
            //     throw new Exception('Update user failed!');
            // }



            $show_user = User::where('user_id', $id)
                ->select('user_id','username', 'first_name', 'last_name', 'email','phone','gender','profile_bio', 'wallet_amount', 'user_image_path')
                ->first();

            $show_address = DB::table('address')
            ->where('user_id', $id)
            ->select('house_no', 'road_no', 'city', 'country')
            ->first();

            return response()->json(array(
                'status' => true,
                'status_message' => "User Update Successful!",
                'user' => $show_user,
                'user_address' => $show_address,
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    //     public function store(Request $request)
    //     {

    //     $validator = Validator::make($request->all(),
    //             [
    //             'user_id' => 'required',
    //             'file' => 'required|mimes:doc,docx,pdf,txt,jpg|max:2048',
    //             ]);

    //     if ($validator->fails()) {
    //             return response()->json(['error'=>$validator->errors()], 401);
    //         }

    //         if ($files = $request->file('file')) {

    //             //store file into document folder
    //             $file = $request->file->store('public/documents');

    //             //store your file into database
    //             $document = new Document();
    //             $document->title = $file;
    //             $document->user_id = $request->user_id;
    //             $document->save();

    //             return response()->json([
    //                 "success" => true,
    //                 "message" => "File successfully uploaded",
    //                 "file" => $file
    //             ]);

    //         }

    //    }

    public function uploadDocument(Request $request)
    {

        try {


            $upload_material = array();
            if ($request->has('lecture_id')) {
                $lecture_id = $request->lecture_id;
                $upload_material['lecture_id'] = $lecture_id;
            }
            if ($request->has('course_id')) {
                $course_id = $request->course_id;
                $upload_material['course_id'] = $course_id;
            }
            if ($request->has('file_name')) {
                $file_name = $request->file_name;
                $upload_material['material_name'] = $file_name;
            }
            // $documentName='';
            $destination = 'files';
            if ($request->has('material_path')) {
                if ($request->hasFile('material_path')) {
                    $document = $request->file('material_path');
                    $documentName = Storage::disk('local')->put($destination, $document);


                    $upload_material['material_path'] = $documentName;
                }

            };

            $upload_document = DB::table('materials')
                ->insert($upload_material);
            if (!$upload_document) {
                throw new Exception('File Uploading failed!');
            }

            return response()->json([
                "success" => true,
                "message" => "Material successfully uploaded",
                "file" => $upload_document,
            ]);
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));

        }
    }

    public function getLecturesByCourses($id){

        try {
            $getLectures=DB::table('lectures')
            ->where('course_id',$id)
            ->get();

            return response()->json([
                "success" => true,
                "message" => "Material successfully uploaded",
                "lectures" => $getLectures,
            ]);

        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }

    }



    public function getTeacherSchedule($id){
        try {
            $now = new DateTime();
            $list2=[];
            $list=[];
        $today=$now->format('Y-m-d');
        // $today=$now->format('Y-m-d');
        $end_range = date('Y-m-d', strtotime('+8 days'));
        $tomorrow_range = date('Y-m-d', strtotime('+1 day'));
        // $end_range=$end_range->format("Y-m-d");
        $get_class_schedule=DB::table('courses')
        ->join('classes','classes.teacher_id', '=' ,'courses.teacher_id')
        ->select('courses.course_id','classes.class_id','courses.course_name','classes.date','classes.start_time','classes.end_time','classes.day')
        ->where('courses.teacher_id',$id)
        ->where('course_status', '=', 'available')
        ->whereBetween('classes.date', [$today, $end_range])
        ->get();

        if (!$get_class_schedule) {
            throw new Exception('classes fetching got failed');
        }


        // for($i=0;$i<sizeof($get_class_schedule);$i++){

        //     }

            foreach ($get_class_schedule as $item) {
                $date = strtotime($item->date);
                $formated_date=date('Y-m-d',$date);
            if($today < $formated_date){
                $list[] = array(
                    'course_id' => $item->course_id,
                    'class_id' => $item->class_id,
                    'course_name' => $item->course_name,
                    'date' => $item->date,
                    'start_time'=> $item->start_time,
                    'end_time'=> $item->end_time,
                    'day'=> $item->day,
                    // 'lectures' => $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]],
                    // 'pending_lectures' => $enrolled_courses[$i]->pending_lectures,
                );
            }
            if($today == $formated_date){
                $list2[] = array(
                    'course_id' => $item->course_id,
                    'class_id' => $item->class_id,
                    'course_name' => $item->course_name,
                    'date' => $item->date,
                    'start_time'=> $item->start_time,
                    'end_time'=> $item->end_time,
                    'day'=> $item->day,
                    // 'lectures' => $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]],
                    // 'pending_lectures' => $enrolled_courses[$i]->pending_lectures,
                );
            }else{
                $list2=[];
            }
            }

        return response()->json(array(
            'status' => true,
            'today' => $list2,
            'teacher_schedule' => $list,
            // 'completed_lectures_of_running_courses' => $lecture_list_completed_g
        ));
      } catch (Exception $e) {
        return response()->json(array(
            'status' => false,
            'status_message' => $e->getMessage(),
        ));
    }
        }




}
