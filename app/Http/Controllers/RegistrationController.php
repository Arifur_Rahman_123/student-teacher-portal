<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Address;
use App\Mail\NotifyMail;
use DB;

use Mail;
use Exception;
use Validator;


class RegistrationController extends Controller{

    public function createUser(Request $request)
    {
        try{
        $request->validate([
            'username' => 'required',
            'password' => 'required',
            'role' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'isPending' => 'required',
        ]);


        
        $first_name = $request -> first_name;
        $last_name = $request -> last_name;
        $username = $first_name." ".$last_name;
        $email = $request -> email;
        $role = $request -> role;

        $get_email=DB::table('users')
        ->select('email')
        ->where('email',$email)
        ->first();

        if($get_email){
           throw new Exception('Email Already exist!!');
        }


        $address['city']=$request ->city;
        $address['house_no']='';
        $address['road_no']='';
        $address['country']='';

        $user = User::create([
            'username' => trim($request->input('username')),
            'email' => strtolower($request->input('email')),
            'password' => bcrypt($request->input('password')),
            'role' => strtolower($request->input('role')),
            'first_name' => strtolower($request->input('first_name')),
            'last_name' => strtolower($request->input('last_name')),
            'phone' => strtolower($request->input('phone')),
            'isPending' => strtolower($request->input('isPending')),

        ]);

        $address_DB = DB::table('address')
        ->insert($address);



        if (!$user) {
            throw new Exception('Create Course failed!');
        }

        if (!$address) {
            throw new Exception('Create Address failed!');
        }



        $first_name = $request -> first_name;
        $last_name = $request -> last_name;
        $full_name=$first_name.' '.$last_name;
        $password = $request -> password;
        if($role == 'student'){
            $email_body="You have been enrolled as a student in hive lms.Please complete your profile after first login";
            Mail::to($email)->send(new NotifyMail($username,$full_name,$email_body,$password));
                if (Mail::failures()) {
                    return response()->Fail('Sorry! Please try again latter');
                }else{
                    return response()->json(array(
                        'status' => true,
                        'status_message' => "Student Create Successful!",
                        'user' => $user,
                        'user_address' => $address,
                    ));
                }
        }


        return response()->json(array(
            'status' => true,
            'status_message' => "User Create Successful!",
            'user' => $user,
            'user_address' => $address,
        ));

    }
    catch (Exception $e) {
        return response()->json(array(
            'status' => false,
            'status_message' => $e->getMessage(),
        ));
    }


    }


}
