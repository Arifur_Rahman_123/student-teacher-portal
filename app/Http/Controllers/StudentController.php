<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Course;
use App\Models\User;
use Exception;
use Datetime;
use Validator;
use DB;


class StudentController extends Controller
{
    /*
    *Student can get enrolled in a specific course through the following method.
    */
    public function postEnrolledValue(Request $request)
    {

        try{
            $rules = array(
                'course_id'  => 'required',
                'student_id'     => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }

            $course_id = $request->course_id;


            $max_students = DB::table('courses')
            ->where('course_id',$course_id)
            ->select('max_students')
            ->get();


            $student_count = DB::table('courses')
            ->where('course_id',$course_id)
            ->select('student_count')
            ->get('course_id',$course_id);

            //if maximum student limit gets reached .We will throw an exception.
            if($max_students == $student_count){
                throw new Exception('Maximum student limit reached already!');
            }

            $insert_course_students['course_id'] = $request->course_id;
            $insert_course_students['student_id'] = $request->student_id;

            $enroll_student = DB::table('course_students')->insert($insert_course_students);
            if (!$enroll_student) {
                throw new Exception('student enroll failed!');
            }

            //updating total student count in the db as we already enrolled one student.

            $increment_student_count_column=DB::table('courses')->increment('student_count');
            $increment_student_count_column=DB::table('courses')->increment('total_selling');
            $teacher_id=DB::table('courses')
                    ->select('teacher_id')
                    ->where('course_id',$course_id)
                    ->get();

            $increment_total_student=DB::table('users')
            ->where('user_id',$teacher_id[0]->teacher_id)
            ->increment('total_students');



            return response()->json(array(
                'status' => true,
                'status_message' => "Student Enrolled Successful!",
                'teacher' => $enroll_student,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }



    /**
     * Student can see their schedule in their dashboard through this method
     */
    public function showEnrolledCourses($id){
        try{

            $show_student = User::where('user_id', $id)
                ->first();

            if (!$show_student) {
                throw new Exception('Student doesnot exist!');
            }



            $enrolled_courses = DB::table('courses')
            ->join('course_students', 'courses.course_id', '=', 'course_students.course_id')
            ->join('users', 'course_students.student_id', '=', 'users.user_id')
            ->join('users as tu', 'courses.teacher_id', '=', 'tu.user_id')
            ->select('courses.course_id','courses.course_name','courses.type','courses.course_image_path','courses.total_lectures',
            'courses.completed_lectures','tu.first_name','tu.last_name','tu.email')
            ->where('users.user_id',$id)
            ->where('course_status', '=', 'available')
            ->get();

            $list=array();
            $course_id_collection=$enrolled_courses->pluck('course_id');
            $result=$enrolled_courses->toArray();
            $lecture_list_completed_g;
            // $lecture_list_pending_p;
            for ($x = 0; $x < sizeof($course_id_collection); $x++) {
                $lecture_list_completed=DB::table('lectures')
                ->select('lecture_id','lecture_title','lecture_image_path','lecture_status')
                ->Where('course_id',$course_id_collection[$x])
                // ->Where('lecture_status','=','completed')
                ->get();
                $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$x]]=$lecture_list_completed;
              }
              for($i=0;$i<sizeof($enrolled_courses);$i++){
                $list[] = array(
                    'course_id' => $enrolled_courses[$i]->course_id,
                    'course_name' => $enrolled_courses[$i]->course_name,
                    'course_image' => $enrolled_courses[$i]->course_image_path,
                    'type' => $enrolled_courses[$i]->type,
                    'teacher_first_name'=> $enrolled_courses[$i]->first_name,
                    'teacher_last_name'=>$enrolled_courses[$i]->last_name,
                    'teacher_email'=>$enrolled_courses[$i]->email,
                    'total_lectures'=> $enrolled_courses[$i]->total_lectures,
                    'completed_lectures'=> $enrolled_courses[$i]->completed_lectures,
                    'lectures' => $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]],
                    // 'pending_lectures' => $enrolled_courses[$i]->pending_lectures,
                );
              }


            if (!$enrolled_courses) {
                throw new Exception('courses fetching got failed');
            }




            return response()->json(array(
                'status' => true,
                'enrolledCourses' => $list,
                // 'completed_lectures_of_running_courses' => $lecture_list_completed_g
            ));


        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /*
    fetching out class schedule
    */

    public function getClassSchedule($id){
      try {

        $now = new DateTime();
        $today=$now->format('Y-m-d');
        // $today=$now->format('Y-m-d');
        $end_range = date('Y-m-d', strtotime('+8 days'));
        $tomorrow_range = date('Y-m-d', strtotime('+1 day'));
        // $end_range=$end_range->format("Y-m-d");
        $get_class_schedule=DB::table('courses')
        ->join('course_students','courses.course_id', '=' ,'course_students.course_id')
        ->join('users', 'course_students.student_id', '=', 'users.user_id')
        ->join('classes','courses.course_id', '=' ,'classes.course_id')
        ->select('courses.course_id','classes.class_id','courses.course_name','classes.date','classes.start_time','classes.end_time','classes.day')
        ->where('users.user_id',$id)
        ->where('course_status', '=', 'available')
        ->whereBetween('classes.date', [$today, $end_range])
        ->get();

        if (!$get_class_schedule) {
            throw new Exception('classes fetching got failed');
        }


        // for($i=0;$i<sizeof($get_class_schedule);$i++){

        //     }

            foreach ($get_class_schedule as $item) {
                $date = strtotime($item->date);
                $formated_date=date('Y-m-d',$date);
            if($today < $formated_date){
                $list[] = array(
                    'course_id' => $item->course_id,
                    'class_id' => $item->class_id,
                    'course_name' => $item->course_name,
                    'date' => $item->date,
                    'start_time'=> $item->start_time,
                    'end_time'=> $item->end_time,
                    'day'=> $item->day,
                    // 'lectures' => $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]],
                    // 'pending_lectures' => $enrolled_courses[$i]->pending_lectures,
                );
            }
            if($today == $formated_date){
                $list2[] = array(
                    'course_id' => $item->course_id,
                    'class_id' => $item->class_id,
                    'course_name' => $item->course_name,
                    'date' => $item->date,
                    'start_time'=> $item->start_time,
                    'end_time'=> $item->end_time,
                    'day'=> $item->day,
                    // 'lectures' => $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]],
                    // 'pending_lectures' => $enrolled_courses[$i]->pending_lectures,
                );
            }else{
                $list2=[];
            }
            }

        return response()->json(array(
            'status' => true,
            'today' => $list2,
            'student_schedule' => $list,
            // 'completed_lectures_of_running_courses' => $lecture_list_completed_g
        ));
      } catch (Exception $e) {
        return response()->json(array(
            'status' => false,
            'status_message' => $e->getMessage(),
        ));
    }
}

    public function showCompletedCourses($id){
        try{

            $show_student = User::where('user_id', $id)
                ->first();

            if (!$show_student) {
                throw new Exception('Student doesnot exist!');
            }



            $enrolled_courses = DB::table('courses')
            ->join('course_students', 'courses.course_id', '=', 'course_students.course_id')
            ->join('users', 'course_students.student_id', '=', 'users.user_id')
            ->join('users as tu', 'courses.teacher_id', '=', 'tu.user_id')
            ->select('courses.course_id','courses.course_name','tu.rating','courses.type','courses.course_image_path','courses.total_lectures',
            'courses.completed_lectures','tu.first_name','tu.last_name','tu.email')
            ->where('users.user_id',$id)
            ->where('course_status', '=', 'completed')
            ->get();

            $course_id_collection=$enrolled_courses->pluck('course_id');
            $result=$enrolled_courses->toArray();
            $lecture_list_completed_g;
            // $lecture_list_pending_p;
            for ($x = 0; $x < sizeof($course_id_collection); $x++) {
                $lecture_list_completed=DB::table('lectures')
                ->select('lecture_id','lecture_title','lecture_image_path','lecture_status')
                ->Where('course_id',$course_id_collection[$x])
                // ->Where('lecture_status','=','completed')
                ->get();

                $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$x]]=$lecture_list_completed;
              }
              for($i=0;$i<sizeof($enrolled_courses);$i++){
                $list[] = array(
                    'course_id' => $enrolled_courses[$i]->course_id,
                    'course_name' => $enrolled_courses[$i]->course_name,
                    'course_image' => $enrolled_courses[$i]->course_image_path,
                    'type' => $enrolled_courses[$i]->type,
                    'total_lectures'=> $enrolled_courses[$i]->total_lectures,
                    'teacher_first_name'=> $enrolled_courses[$i]->first_name,
                    'teacher_last_name'=> $enrolled_courses[$i]->last_name,
                    'teacher_email'=> $enrolled_courses[$i]->email,
                    'completed_lectures'=> sizeof($lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]]),
                    'lectures' => $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]],
                    // 'pending_lectures' => $enrolled_courses[$i]->pending_lectures,
                );
              }



            // $course_id=$enrolled_courses->toArray();
            if (!$enrolled_courses) {
                throw new Exception('courses fetching got failed');
            }




            return response()->json(array(
                'status' => true,
                'completedCourses' => $list,
                // 'completed_lectures_of_running_courses' => $lecture_list_completed_g
            ));


        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }


    }

    public function downloadDocument($id)
    {
        try{
        $material = DB::table('materials')
        ->Where('material_id',$id)
        ->select('material_path')
        ->first();
        $material_path=$material->material_path;
        // return response()->json(array(
        //     'status' => true,
        //     'enrolledCourses' => $material_path,
        //     // 'completed_lectures_of_running_courses' => $lecture_list_completed_g
        // ));
        return response()->download($material_path);
        }catch(Exception $e){
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }


    public function getMaterialsByLectureId($id){
        try{
            $material_list = DB::table('materials')
            ->join('courses','courses.course_id','materials.course_id')
            ->select('courses.course_id','courses.course_image_path','courses.course_name','materials.material_id','materials.lecture_id','materials.material_path','materials.material_name')
            ->where('materials.lecture_id',$id)
            ->get();

            if(!$material_list){
                throw new Exception('Material fetching Got failed');
            }
            return response()->json(array(
                'status' => true,
                'material_list' => $material_list,
            ));

        }catch(Exception $e){
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }


    public function postComment(Request $request){

        $rules = array(
            'course_id'  => 'required',
            'user_id'  => 'required',
            'comment_text'     => 'required',
            'rating'     => 'required',
        );

    try{
        $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }

        $course_id = $request->course_id;

        $post_comment['course_id']=$course_id;
        $post_comment['user_id']=$request->user_id;
        $post_comment['comment_text']=$request->comment_text;
        $post_comment['rating']=$request->rating;
        $post_comment_DB=DB::table('comments')
        ->insert($post_comment);

        if(!$post_comment_DB){
            throw new Exception('post comment failed');
        }

        return response()->json(array(
            'status' => true,
            'comment' => $post_comment_DB,
        ));
    }catch(Exception $e){
        return response()->json(array(
            'status' => false,
            'status_message' => $e->getMessage(),
        ));
    }


    }

    public function postFeedBack(Request $request){

        $rules = array(

            'user_id'         => 'required',
            'email'           => 'required',
            'subject'         => 'required',
            'feedback_details'=> 'required',
        );

        try{
            $validator = Validator::make($request->all(), $rules);
                if (!$validator->passes()) {
                    throw new Exception('All fields are required');
                }

            $course_id = $request->course_id;
            $post_feedback['user_id']=$request->user_id;
            $post_feedback['email']=$request->email;
            $post_feedback['subject']=$request->subject;
            $post_feedback['feedback_details']=$request->feedback_details;
            $post_feedback_to_db=DB::table('feedback')
            ->insert($post_feedback);

            if(!$post_feedback_to_db){
                throw new Exception('post feedback failed');
            }

            return response()->json(array(
                'status' => true,
                'feeback' => $post_feedback_to_db,
            ));
        }catch(Exception $e){
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }

    }

    public function getFeedBacks(){
        try{
            $get_all_feedBacks=DB::table('feedback')
            ->get();

            if(!$get_all_feedBacks){
                throw new Exception('post feedback failed');
            }

            return response()->json(array(
                'status' => true,
                'feebacks' => $get_all_feedBacks,
            ));
        }catch(Exception $e){
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    public function getTestQuestions($id){
        try {
            $test_questions=DB::table('quiz')
            ->join('questions','questions.quiz_id','=','quiz.quiz_id')
            ->select('quiz.quiz_id','quiz.course_id','quiz.lecture_id','questions.question_id','questions.question','questions.answer','questions.time','questions.first_option','questions.second_option','questions.third_option','questions.fourth_option')
            ->where('quiz.lecture_id',$id)
            ->get();

            return response()->json(array(
                'status' => true,
                'feebacks' => $test_questions,
            ));
        } catch(Exception $e){
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }


    /**
     * Posting quiz result from a question
     */

     public function postQuizResults(Request $request){
        $rules = array(
            'student_id'          => 'required',
            'course_id'           => 'required',
            'lecture_id'          => 'required',
            'quiz_id'             => 'required',
            'total_right_answer'  => 'required',
            'total_wrong_answer'  => 'required',
            'total_questions'     => 'required',
        );

        try{
            $validator = Validator::make($request->all(), $rules);
                if (!$validator->passes()) {
                    throw new Exception('All fields are required');
                }

            $course_id = $request->course_id;
            $student_id=$request->student_id;
            $quiz_id=$request->quiz_id;
            $lecture_id=$request->lecture_id;
            $total_q=$request->total_questions;
            $total_r=$request->total_right_answer;
            $availed_marks=($total_r/$total_q)*100;
            $insert_result['student_id']=$student_id;
            $insert_result['course_id']=$course_id;
            $insert_result['quiz_id']=$quiz_id;
            $insert_result['lecture_id']=$lecture_id;
            $insert_result['total_right_answer']=$total_r;
            $insert_result['total_wrong_answer']=$request->total_wrong_answer;
            $insert_result['total_questions']=$total_r;
            $insert_result['availed_marks']=$availed_marks;
            $insert_result['is_completed']=1;


            $retake_checking=DB::table('quiz_results')
            ->where('student_id','=',$student_id)
            ->where('quiz_id','=',$quiz_id)
            ->where('lecture_id','=',$lecture_id)
            ->where('course_id','=',$course_id)
            ->get();

            $retake_checking_array=$retake_checking->toArray();

           $result_id=$retake_checking->pluck('result_id');

            if($retake_checking_array==[]){
                $insert_result_to_db=DB::table('quiz_results')
                ->insert($insert_result);
            }else{
                $insert_result_to_db=DB::table('quiz_results')
                ->where('result_id',$result_id)
                ->update($insert_result);

            }
            if(!$insert_result_to_db){
                throw new Exception('post result failed');
            }


            return response()->json(array(
                'status' => true,
                'result' => $insert_result_to_db,
            ));
        }catch(Exception $e){
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
     }

     /**
      * achievements
      *,'courses.certificate','courses.completed_badge'
      */


      public function getAchievements($id){

        try{

            $show_student = User::where('user_id', $id)
                ->first();

            if (!$show_student) {
                throw new Exception('Student doesnot exist!');
            }



            $enrolled_courses = DB::table('courses')
            ->join('course_students', 'courses.course_id', '=', 'course_students.course_id')
            ->join('users', 'course_students.student_id', '=', 'users.user_id')
            ->select('courses.course_id','courses.course_name','courses.course_image_path','courses.certificate','courses.completed_badge')
            ->where('users.user_id',$id)
            ->where('course_status', '=', 'completed')
            ->get();
            //   for($i=0;$i<sizeof($enrolled_courses);$i++){
            //     $list[] = array(
            //         'course_id' => $enrolled_courses[$i]->course_id,
            //         'course_name' => $enrolled_courses[$i]->course_name,
            //         'course_image' => $enrolled_courses[$i]->course_image_path,
            //         'type' => $enrolled_courses[$i]->type,
            //         'total_lectures'=> $enrolled_courses[$i]->total_lectures,
            //         'completed_lectures'=> sizeof($lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]]),
            //         'lectures' => $lecture_list_completed_g['completed_list_of_lectures_from_course_id_'.$course_id_collection[$i]],
            //         // 'pending_lectures' => $enrolled_courses[$i]->pending_lectures,
            //     );
            //   }



            // $course_id=$enrolled_courses->toArray();
            if (!$enrolled_courses) {
                throw new Exception('courses fetching got failed');
            }




            return response()->json(array(
                'status' => true,
                'achievements' => $enrolled_courses,
                // 'completed_lectures_of_running_courses' => $lecture_list_completed_g
            ));


        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }


    }


    public function getAllstudents()
    {
        try {
            $show_all_teachers = DB::table('users')
                ->join('address','address.user_id','=','users.user_id')
                ->select('users.user_id','users.username', 'users.first_name','users.user_image_path','users.wallet_amount','users.teacher_designation', 'users.last_name', 'users.email', 'users.phone', 'address.house_no','address.road_no','address.city','address.country')
                ->where('role', 'student')
                ->get();
            if (!$show_all_teachers) {
                throw new Exception('User doesnot exist!');
            }
            return response()->json(array(
                'status' => true,
                'students' => $show_all_teachers,
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

   public function getQuizRankings($id){

        try {
            $get_quiz_results=DB::table('quiz_results')
            ->select('users.first_name','users.last_name','users.user_image_path','users.user_id',DB::raw('SUM(quiz_results.availed_marks) as total_marks'))
            ->join('users','users.user_id','=','quiz_results.student_id')
            ->where('quiz_results.course_id',$id)
            ->groupBy('quiz_results.student_id')
            ->orderBy('total_marks', 'desc')
            ->paginate(10);

            $result = $get_quiz_results->toArray();
            if (!$get_quiz_results) {
                throw new Exception('User doesnot exist!');
            }
            return response()->json(array(
                'status' => true,
                'current_page' => $result["current_page"],
                'total_page' => $result["last_page"],
                'students' => $result["data"],
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

}
