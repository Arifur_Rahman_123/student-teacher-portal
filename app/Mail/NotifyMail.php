<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyMail extends Mailable
{
    use Queueable, SerializesModels;

     public $username;
     public $email_body;
     public $full_name;
     public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username,$email_body,$full_name,$password)
    {
        $this->username=$username;
        $this->email_body=$email_body;
        $this->full_name=$full_name;
        $this->password=$password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Mail from lms')->view('emails.demoMail');
    }
}
