@extends('master')

@section('Title')
    All Courses
@endsection

@section('Style')


@endsection

@section('Content')
<div class="card card-box" style="height: 80px;">

    <nav aria-label="breadcrumb" style="margin-bottom: 10px;margin-left: 20px;">
      <ol class="breadcrumb" style="margin-top: 10px">
        <li class="breadcrumb-item"><a >Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Enrolled-Courses</li>
      </ol>
    </nav>
    <div class="card-body">
  
     </div>
  </div>
<div class="pb-20" style="margin-top: 20px">
    <div class="card-box mb-30">
        <div class="pd-20">
            <h4 class="text-blue h4">Enrolled Courses List</h4>
            {{-- <p class="mb-0">you can find more options</p> --}}
        </div>

        <div class="pd-20">
            <table id="dataTable" class="table hover table stripe data-table-export nowrap">
                <thead>
                </thead>
            </table>
        </div>

        
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        retrieve: true,
        pageLength: 25,
        language: {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        columns: [
        {'title':'#',data: 'DT_RowIndex'},
        {'title':'Username',data: 'course_name'},
        {'title':'Hive ID',data: 'course_price'},
        {'title':'Country',data: 'course_description'},
        ]
        });
        });
    </script>
@endsection

@section('Script')

@endsection