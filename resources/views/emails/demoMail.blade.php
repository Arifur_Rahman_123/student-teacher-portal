<!DOCTYPE html>
<html>
<head>
 <title>Verify Account</title>
<body>

 <h1>Congratulations!!Your Account Has Been Verified</h1>
 <br>
 <span>Dear {{$full_name}},</span>
 <br>
 <p>{{$email_body}}</span>
 <p>Your User Name is {{$username}}</p>
 <p>Your password is {{$password}}</p>
 <h3 style="color: red;font-weight: bold">Do not Share your password with anyone</h3>
 <br>
 <br>
 <p>Regards,</p>
 <p>Hive Technical Team</p>

</body>
</html>
