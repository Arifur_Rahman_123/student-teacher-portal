<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\SendEmailController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group([
    'middleware' => 'api',
    // 'prefix' => 'auth'

    //displayCourseFromSearchKey
], function ($router) {
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/logout', [LoginController::class, 'logout']);
    Route::post('/create-user', [RegistrationController::class, 'createUser']);
    Route::post('/refresh', [LoginController::class, 'refresh']);
    Route::get('/user-profile', [LoginController::class, 'userProfile']);
    Route::post('/change-password', [TeacherController::class, 'updatePassword']);



    Route::get('/courses', [CourseController::class, 'showAllCourses']);
    // Route::post('/courses', [CourseController::class, 'createCourse']);
    Route::get('/courses/course-search-result/{searchKey}', [CourseController::class, 'displayCourseFromSearchKey']);
    Route::get('/courses/{id}', [CourseController::class, 'displayCourseDetails']);
    Route::get('/enrolled-courses/{id}', [StudentController::class, 'showEnrolledCourses']);
    Route::get('/taught-courses/{id}', [TeacherController::class, 'showTeachingCourses']);
    Route::get('/running-courses/{id}', [TeacherController::class, 'showRunningCourses']);
    Route::post('/upload', [TeacherController::class, 'uploadDocument']);
    Route::get('/get-all-teacher', [TeacherController::class, 'getAllTeachers']);
    Route::post('/create-course', [CourseController::class, 'createCourse']);
    Route::get('/categories', [CourseController::class, 'displayCoursesCountByCategory']);
    Route::get('/categories/{id}', [CourseController::class, 'displayCoursesByCategory']);
    // Route::post('/calculate-classes', [CourseController::class, 'calculateClasses']);
    // Route::get('/send-email', [SendEmailController::class, 'sendEmail']);
    Route::get('/class-schedule/{id}', [StudentController::class, 'getClassSchedule']);
    Route::get('/completed-courses/{id}', [StudentController::class, 'showCompletedCourses']);
    Route::get('/get-all-courses', [CourseController::class, 'showAllCoursesForWeb']);
    Route::get('/running-courses/{id}', [TeacherController::class, 'showRunningCourses']);

    //routes for admin
    Route::post('/create-new-user', [UserController::class, 'createNewUser']);



    //routes for App or Student Portal Web

    Route::post('/post-comment', [StudentController::class, 'postComment']);
    Route::post('/post-feedback', [StudentController::class, 'postFeedBack']);
    Route::post('/post-quiz-results', [StudentController::class, 'postQuizResults']);
    Route::get('/completed-courses/{id}', [StudentController::class, 'showCompletedCourses']);
    Route::get('/achievements/{id}', [StudentController::class, 'getAchievements']);


    //routes for admin and teacher
    Route::get('/get-all-feedbacks', [StudentController::class, 'getFeedBacks']);
    Route::post('/upload-document', [TeacherController::class, 'uploadDocument']);
    //getAllstudents
    Route::get('/get-all-students', [StudentController::class, 'getAllstudents']);

    //common routes
    Route::post('/update-profile/{id}', [TeacherController::class, 'updateProfile']);
    Route::get('/get-user/{id}', [UserController::class, 'getUser']);
    Route::get('/get-materials/{id}', [StudentController::class, 'getMaterialsByLectureId']);
    Route::post('/enroll-student', [StudentController::class, 'postEnrolledValue']);//enrolling in a course
    Route::get('/download-document/{id}', [StudentController::class, 'downloadDocument']);
    Route::get('/quiz-questions/{id}', [StudentController::class, 'getTestQuestions']);
    Route::delete('/delete-user/{id}', [UserController::class, 'deleteUser']);
    Route::post('/dispatch-teacher', [UserController::class, 'dispatchTeacher']);
    Route::get('/get-quiz-ranking/{id}', [StudentController::class, 'getQuizRankings']);
    Route::get('/get-top-courses', [CourseController::class, 'showTopCourses']);
    Route::get('/get-lectures/{id}', [TeacherController::class, 'getLecturesByCourses']);




    ///route for web
    Route::get('/get-teacher/{id}', [UserController::class, 'getTeacher']);
    Route::get('/get-student/{id}', [UserController::class, 'getStudent']);
    Route::get('/get-teacher-schedule/{id}',[TeacherController::class, 'getTeacherSchedule']);


}
);
